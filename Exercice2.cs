using System;

public class MoreMath
{
    public void Prime()
    {

        for (int i = 1; i < 1000; i++)
        {
            bool prime = true;
            for (int j = 2; j < System.Math.Sqrt(i) + 1; j++)
            {
                if ((i % j) == 0)
                {
                    prime = false;
                    break;
                }
            }
            if (prime)
            {
                Console.Write("[ " + i + " ]  ");
            }

        }

        Console.WriteLine("\n");
    }

    public static int AskUserForParameter()
    {
        Console.WriteLine("Please write a number for the function and press enter :");
        int.TryParse(Console.ReadLine(), out var result);
        return result;
    }
    public void Fibonacci()
    {
        Console.WriteLine("\nFibonacci Function :");
        int number = AskUserForParameter();

        while (number == 0)
        {
            Console.WriteLine("Error, please chose a VALID number");
            number = AskUserForParameter();
        }

        int Nminus1 = 1;
        int Nminus2 = 1;
        int inter;
        for (int i = 0; i < 2; i++)
        {
            Console.WriteLine("F(" + i + ") = " + 1);

        }
        for (int i = 2; i < number + 1; i++)
        {
            Console.WriteLine("F(" + (i) + ") = " + (Nminus1 + Nminus2));
            inter = Nminus1 + Nminus2;
            Nminus2 = Nminus1;
            Nminus1 = inter;

        }


    }

    public void FactorialFunction()
    {
        Console.WriteLine("\nFactorial Function :");
        long number = AskUserForParameter();

        while (number == 0)
        {
            Console.WriteLine("Error, please chose a VALID number");
            number = AskUserForParameter();
        }
        Console.WriteLine("Result :\n " + number + "! = " + Multi(number));
    }

    private long Multi(long a)
    {
        if (a > 1)
        {
            return (a * Multi(a - 1));
        }
        else
        {
            return 1;
        }
    }
}