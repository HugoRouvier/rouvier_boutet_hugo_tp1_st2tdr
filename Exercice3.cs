using System;

public class TryCatch
{
    private static int PowerFunction(int x)
    {
        return (int)(Math.Pow(x, 2) - 4);
    }

    public void TC()
    {
        for (int i = (-3); i < 4; i++)
        {
            try
            {
                Console.WriteLine("Step " + (i + 3) + " :");
                Console.WriteLine("Result = " + (10 / PowerFunction(i)));
            }
            catch (System.DivideByZeroException e)
            {
                Console.WriteLine(e);
            }
        }


    }
}