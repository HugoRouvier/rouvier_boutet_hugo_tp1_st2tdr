using System;

namespace TP1C_
{
    public class Mult
    {
        public void printM(bool single = false, int number = 11, int odd = 1)
        {
            for (int i = 1; i < 11; i++)
            {
                if (single == false)
                {
                    for (int j = 1; j < 11; j++)
                    {
                        if (odd == 2)
                        {
                            if ((i * j) % 2 == 1)
                            {
                                Console.Write(i + " * " + j + " = " + i * j + "  ");
                            }
                        }
                        else
                        {
                            Console.Write(i + " * " + j + " = " + i * j + "  ");
                        }

                    }
                }
                else
                {
                    Console.Write(i + " * " + number + " = " + i * number + "  ");
                }

                Console.Write("\n");
            }
        }
        public static int AskUserForParameter()
        {
            Console.WriteLine("Please write a number and press enter :");
            int.TryParse(Console.ReadLine(), out var result);
            return result;
        }

        public void exercice1()
        {
            Console.WriteLine("\nTable of multiplication : \n");
            printM();
            int number = AskUserForParameter();
            bool single = true;
            if (number == 0)
            {
                number = 11;
                single = false;
            }

            Console.WriteLine("\nTable of " + number + " : \n");
            printM(single, number);

            Console.WriteLine("\nTable of odd numbers : \n");
            printM(false, 11, 2);
        }


    }
}