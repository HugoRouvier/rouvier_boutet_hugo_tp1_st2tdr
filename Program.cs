﻿using System;

namespace TP1C_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-----Run Exercice 1-----\n");
            Mult Ex1 = new Mult();
            Ex1.exercice1();
            Console.WriteLine("-----Fin Exercice 1-----\n");

            Console.WriteLine("-----Run Exercice 2-----\n");
            MoreMath Ex2 = new MoreMath();
            Ex2.Prime();
            Ex2.Fibonacci();
            Ex2.FactorialFunction();
            Console.WriteLine("-----Fin Exercice 2-----\n");
            
            Console.WriteLine("-----Run Exercice 3-----\n");
            TryCatch Ex3 = new TryCatch();
            Ex3.TC();
            Console.WriteLine("-----Fin Exercice 3-----\n");

            Console.WriteLine("-----Run Exercice 4-----\n");
            Fun Ex4 = new Fun();
            Ex4.Square();
            Console.WriteLine("-----Fin Exercice 4-----\n");

            Console.WriteLine("-----Run Exercice 5-----\n");
            Tree Ex5 = new Tree();
            Ex5.Regular();
            Console.WriteLine("-----Fin Exercice 5-----\n");
        }
    }
}
