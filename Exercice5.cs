using System;

public class Tree
{
    public void Regular()
    {
        int size = AskUserForParameter();
        int tracker = 1;
        int trackerSize = size;
        for (int i = 0; i < size + 1; i++)
        {
            if (i == size)
            {
                for (int j = 0; j < size - 2; j++)
                {
                    Console.Write(" ");
                }
                Console.Write("| |");
            }
            else
            {
                for (int j = 0; j < trackerSize - 1; j++)
                {
                    Console.Write(" ");
                }
                for (int j = 0; j < tracker; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine("");
                trackerSize--;
                tracker = tracker + 2;
            }

        }
        Console.WriteLine("");
        int choice = AskUserForChoice();
        if (choice == 1)
        {
            Decorated(size);
        }
        Console.WriteLine("");
    }

    public void Decorated(int size)
    {
        int trackerSize = size;
        int trackerSpace = 1;
        int trackerLargeur = 1;
        int trackerLine = 0;
        for (int i = 0; i < size + 1; i++)
        {
            if (i == size)
            {
                for (int j = 0; j < size - 2; j++)
                {
                    Console.Write(" ");
                }
                Console.Write("| |");
            }
            else
            {
                for (int j = 0; j < trackerSize - 1; j++)
                {
                    Console.Write(" ");
                }

                for (int j = 0; j < trackerLargeur; j++)
                {
                    if (trackerSpace == 0)
                    {
                        Random deco = new Random();
                        if (deco.Next(0, 10) < 7)
                        {
                            Console.Write("i");
                        }
                        else
                        {
                            Console.Write("o");
                        }
                        trackerSpace = trackerSpace + 2;
                    }
                    else
                    {
                        Console.Write("*");
                        trackerSpace--;
                    }
                }
                trackerLine++;
                if (trackerLine == 2)
                {
                    trackerSpace = 2;
                }
                if (trackerLine == 3)
                {
                    trackerSpace = 1;
                    trackerLine = 0;
                }


                Console.WriteLine("");
                trackerSize--;
                trackerLargeur = trackerLargeur + 2;
            }
        }
    }

    public static int AskUserForParameter()
    {
        Console.WriteLine("Please write a size for your tree and press enter :");
        int.TryParse(Console.ReadLine(), out var result);

        while (result < 3 || result > 20)
        {

            Console.WriteLine("Please try again and write a number N >= 1 and press enter :");
            int.TryParse(Console.ReadLine(), out result);
        }

        return result;
    }


    public static int AskUserForChoice()
    {
        Console.WriteLine("Please write if you want to decorate your tree (enter 1) or not (enter 2) and press enter :");
        int.TryParse(Console.ReadLine(), out var result);

        while (result != 1 && result != 2)
        {
            Console.WriteLine("Please try again and write a number N = 1  or N = 2 and press enter :");
            int.TryParse(Console.ReadLine(), out result);
        }

        return result;
    }
}