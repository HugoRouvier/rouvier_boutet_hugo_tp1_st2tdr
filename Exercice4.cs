using System;
 
 public class Fun{

     public static (int,int) AskUserForParameter()
    {
        Console.WriteLine("Chose parameters for your rectangle :\n");
        Console.WriteLine("Please write a number N and press enter :");
        int.TryParse(Console.ReadLine(), out var result);

        Console.WriteLine("Please write a number M and press enter :");
        int.TryParse(Console.ReadLine(), out var result1);

        while (result < 1)
        {

            Console.WriteLine("Please try again and write a number N >= 1 and press enter :");
            int.TryParse(Console.ReadLine(), out result);
        }
        while (result1 > 1000)
        {
            Console.WriteLine("Please try again and write a number M <= 1000 and press enter :");
            int.TryParse(Console.ReadLine(), out result1);             
        }

        return (result , result1);
    }
     public void Square(){
         var size = AskUserForParameter();
         int N = size.Item1;
         int M = size.Item2;
         int trackerSpace = 0;
         for (int i = 1; i < M + 1; i++)
         {
             if (trackerSpace == 3)
             {
                 trackerSpace = 0;
             }
             if (i == 1 || i == M)
             {
                 drawEndsLines(N);
             }
             else
             {
                 drawCoreLines(N, trackerSpace);
                 trackerSpace++;
             }
         }     
     }

     private void drawEndsLines(int N){
         for (int i = 1; i < N + 1; i++)
         {
             if (i == 1 || i == N)
             {
                 Console.Write("0");
             }
             else
             {
                 Console.Write("-");                 
             }
             Console.Write("");
         }
         Console.WriteLine("");
     }

     private void drawCoreLines(int N, int trackerSpace)
     {
         for (int i = 1; i < N + 1; i++)
         {
             if (i == 1 || i == N)
             {
                 Console.Write("|");
             }
             else
             {
                 if (trackerSpace == 0)
                 {
                    Console.Write("*");
                    trackerSpace = trackerSpace + 2;
                 }
                 else
                 {
                     Console.Write(" ");
                     trackerSpace --;
                 }
                                  
             }
             Console.Write("");
         }
         Console.WriteLine("");
     }
 }